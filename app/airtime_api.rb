require './app/libraries'

class AirtimeApi < Sinatra::Base

  set :public_folder, 'public'

  Rabl.register!

  Rabl.configure do |config|
    #config.include_json_root = true
    config.json_engine = ::Oj
  end

  $db = URI.parse(ENV['DATABASE_URL'] || 'postgres://airtime:airtime@localhost:5432/airtime')

  ActiveRecord::Base.establish_connection(
    :adapter  => $db.scheme == 'postgres' ? 'postgresql' : $db.scheme,
    :host     => $db.host,
    :port     => $db.port,
    :username => $db.user,
    :password => $db.password,
    :database => $db.path[1..-1],
    :encoding => 'utf8'
  )

  before do
    content_type :html
  end

end
