collection @upcoming_tracks, :root => false, :object_root => false

code :track do |s|
  "#{s.file.artist_name} - #{s.file.track_title}"
end
