class AirtimeApi < Sinatra::Base

  get "/?" do
    "no"
  end

  get "/current_track" do
    @current_track = Airtime.current_track
    render :rabl, :current_track, :format => "json"
  end

  get "/upcoming_tracks" do
    @upcoming_tracks = Airtime.upcoming_tracks
    render :rabl, :upcoming_tracks, :format => "json"
  end

end
