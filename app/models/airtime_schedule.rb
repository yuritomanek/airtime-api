class AirtimeSchedule < ActiveRecord::Base
  self.table_name = "cc_schedule"

  belongs_to :file, :class_name => "AirtimeFile", :foreign_key => "file_id"

end
