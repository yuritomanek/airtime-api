class Airtime

  def self.current_track
    s = AirtimeSchedule.where("starts <= ? AND ends > ? AND media_item_played = ?", Time.now, Time.now, true).limit(1).first
    if s
      file = s.file
      return "#{file.artist_name} - #{file.track_title}"
    else
      return "The station is quiet. On the air soon."
    end
  end

  def self.upcoming_tracks
    AirtimeSchedule.where("starts >= ? AND media_item_played = ?", Time.now, false).order("starts ASC").limit(5)
  end

end
