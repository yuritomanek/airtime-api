worker_processes 4
working_directory "/opt/www/current"

listen 8080, :tcp_nopush => true

timeout 30

pid "/opt/www/shared/pids/unicorn.pid"

stderr_path "/opt/www/shared/log/unicorn.stderr.log"
stdout_path "/opt/www/shared/log/unicorn.stdout.log"

preload_app true
GC.respond_to?(:copy_on_write_friendly=) and
  GC.copy_on_write_friendly = true

check_client_connection false
