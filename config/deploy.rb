require "bundler/capistrano"

set :scm, :git
set :repository, "git@git.altlabs.com.au:altlabs/airtime-api.git"

default_run_options[:pty] = true
set :ssh_options, {:forward_agent => true}
set :user, "deploy"

set :deploy_via, :remote_cache
set :deploy_to, "/opt/www"

role :web, "admin.soundintroversion.com"
role :app, "admin.soundintroversion.com"
role :db, "admin.soundintroversion.com", :primary => true

set :rails_env, :production
set :unicorn_binary, "/usr/local/bin/unicorn"
set :unicorn_config, "#{current_path}/config/unicorn.conf.rb"
set :unicorn_pid, "/opt/www/shared/pids/unicorn.pid"

namespace :deploy do

  task :start, :roles => :web, :except => { :no_release => true } do
    run "cd #{current_path} && bundle exec #{unicorn_binary} -c #{unicorn_config} -E #{rails_env} -D"
  end

  task :stop, :roles => :web, :except => { :no_release => true } do
    run "#{try_sudo} kill `cat #{unicorn_pid}`"
  end

  task :graceful_stop, :roles => :web, :except => { :no_release => true } do
    run "#{try_sudo} kill -s QUIT `cat #{unicorn_pid}`"
  end

  task :restart, :roles => :web, :except => { :no_release => true } do
    run "#{try_sudo} kill -s USR2 `cat #{unicorn_pid}`"
  end

  task :forece_restart, :roles => :web, :except => { :no_release => true } do
    stop
    start
  end

end

after "deploy:update", "deploy:cleanup"
